//Setup dependencies
const express = require("express");
const mongoose = require('mongoose');
//allows our backend application to connect to  our frontend application
//allows us to control the application's cross- origin resource sharing setting
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js")
//Setup server
//create an 'app' variable taht stores the result of the "express" function that allow us access to different methods that will make the backed creation easy
const app = express();

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Database connection
mongoose.connect("mongodb+srv://admin:admin1234@b256-tayco.sgfs3qo.mongodb.net/B256_CourseAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

//Routes
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));



// server listening
app.listen(4000, () => console.log(`API is now online on port 4000`));


