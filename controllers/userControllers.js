// contains all the business logic and functions of our application
// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User.js");
const Course = require("../models/Course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
//Check if email already exists
/*
	Business Logic: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkIfEmailExists = (requestBody) => {

	return User.find({email: requestBody.email}).then(result => {

		if(result.lenght > 0) {

			return true;

		} else {

			return false;
		}
	})
};

//User Registeration
/*
	BUsiness Logic:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (requestBody) => {

	let newUser = new User ({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err) {
			return false;

		} else {
			return true;
		}
	})

};

// User authentication
		/*
			Steps:
			1. Check the database if the user email exists
			2. Compare the password provided in the login form with the password stored in the database
			3. Generate/return a JSON web token if the user is successfully logged in and return false if not
		*/

module.exports.authenticateUser = (requestBody) => {

	return User.findOne({email:requestBody.email}).then(result => {

		if(result == null) {

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			if(isPasswordCorrect){

				return {access: auth.createAccessToken(result)}

			} else {

				return false;
			}
		}
	})
};

//Activity S38

module.exports.getProfile = (paramsId, requestBody)=> {
   
   return User.findOne({id:paramsId}).then((result, err) => {

   		if(err){
   			console.log(error);
   			return "User not found";

   		} else {

   			result.password = " ";

   			return result;

   		}
   })
};

//Enrolling a uer to a course

/*
	Business Logic:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enrollUser = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((enrolled, err) => {

			if(err){

				return false;

			} else {

				return true
			}
		})

	});


	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

	 	course.enrollees.push({userId: data.userId});

	 	return course.save().then((enroll, err) =>{

	 		if(err){

	 			return false;

	 		} else {

	 			return true;

	 		}
	 	})
	 });

	if(isUserUpdated && isCourseUpdated) {


		return true;
	} else {

		return false;
	}


}


module.exports.getAllUsers= () => {

	return User.find({}).then(result => {
		return result;
	})
}



module.exports.getNonAdmin = () => {

	return User.find({isAdmin: false}).then(result => {

		return result;
	});
};


