const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js")


//Route for checking if the user's email already exists in the database 

router.post("/checkEmail", (req, res) => {

	userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});


//Router for user registration

router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//router for user authentication 

router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController))
});

//Activity S38
router.post("/details", (req, res) => {

	userController.getProfile(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
});



//Route for enrolling a user

router.post("/enroll", auth.verify, (req, res) => {

	const data = {
		//userId will be received from the request header
		userId: auth.decode(req.headers.authorization).id,
		courseId:req.body.courseId
	}

	userController.enrollUser(data).then(resultFromController => res.send(resultFromController));
	
})


//Reterieving all users

router.get("/allusers",( req, res) => {

	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
})


//Route for retrieveing all non-admin users

router.get("/nonadmin", (req, res) => {

	userController.getNonAdmin().then(resultFromController => res.send(resultFromController))
})
module.exports = router;



